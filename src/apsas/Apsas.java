/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apsas;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;


/**
 *
 * @author Fractal
 */
public class Apsas {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        People pr = new People();
        pr.setFirstName("Tomas");
        pr.setLastName("Alibaba");
        pr.setBirth_date(Date.valueOf("2003-10-12"));
        pr.setSalary(BigDecimal.valueOf(10));
        
        
        
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("ApsasPU");
        EntityManager em = emf.createEntityManager();
//        People person = em.find(People.class, 2);
//        People kk = em.find(People.class, 9);
        EntityTransaction tx = em.getTransaction();
                tx.begin();
                tx.commit();
//        em.persist(pr);
//        em.remove(kk);
//        tx.commit();
//        tx.begin();   objekto pakelimas ir pakeitimas
//        People p = em.find(People.class, 2);
//        p.setFirstName("Tadas");
//        tx.commit();
        tx.begin();   
        People p = em.find(People.class, 1);
        System.out.println(p.getAddress());
        Address ad = new Address();
        ad.setAddress("Jonavos g. 25");
        ad.setCity("Kaunas");
        ad.setPc("46644");
        
        ad.setPeopleId(p);
        
        em.persist(ad);
        tx.commit();
//        System.out.println(person);
//        Query q = em.createQuery("SELECT p FROM People p");
//        List rl = q.getResultList();
//        System.out.println(rl);
        em.close();
        emf.close();
    }
    
}
